# Spring cloud config service

## Depenedencies

```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-config-server</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-bootstrap</artifactId>
    </dependency>
```
## Configuration

1. Create a file called `bootstrap.yml` under `src/main/resources` directory
2. Add the configuration inside the `bootstrap.yml` file
```yml
spring:
  application:
    name: configservice
```
3. In the `application.yml` file
```yaml
server:
  port: 8888
```
```yml
spring:
  profiles:
    active: git
  cloud:
    config:
      server:
        git:
          uri: https://gitlab.com/classpath-spring-microservices/orderservice.git
          repos:
            inventoryservice:
              uri: https://gitlab.com/classpath-spring-microservices/inventoryservice.git
              clone-on-startup: true
```

3. Enable the config service
In the root file add the `@EnableConfigServer` annotation

4. To encrypt sensitive data at rest add the below configuration in `bootstrap.yml` file
```yml
encrypt:
  key: SOME_SUPER_SENSITIVE_SECRET_KEY
```
5. Encrypt the sensitive data using 

```
POST request - http://localhost:8888/encrypt
raw: text

POST request - http://localhost:8888/decrypt
raw: text
```

   
### Test the application
4. Start the config service and ensure that the service is running on `8888` port
5. Verify the configuration by running the following `HTTP` `GET` request

```http request
http://localhost:8888/orderservice/dev
http://localhost:8888/inventoryservice/dev
```
